from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'My name is Wurie (usually called Wru). I like classic novel, poem, astronomy, anything colored black. National Geographics and Times are my daily dose of articles. I currently study computer science in university. For my side activities I like to figure why, how things exist, how they survive, how they might feel, what future they possibly experience. My strange behavior is that sometimes I dreamt about random things come true (I don’t know, people called it de javu). I hate the part when I forgot what I have dreamt about after I wake up.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)